#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 21:58:41 2021

@author: kko033
"""

import proplot as plot
import pandas as pd
import numpy as np

def _loadDefaults_():
    import glob
    _places = glob.glob('./default_data/*.p')
    default_places = {}
    for place in _places:
        namePlace = place.split('/')[-1][0:-2]
        default_places[namePlace] = pd.read_pickle(place)
    return default_places

def _prepareData_(sunny_df):
    from datetime import datetime as dt

    s_df= sunny_df.assign(doy = lambda x:x.index.dayofyear)

    _time = [dt.timestamp(ii) for ii in s_df.index]
    _sun = s_df.sun.values.astype(float)
    _ze  = s_df.ze.values.astype(float)

    s_df['timestamp'] = _time


    actual_sun_daily = np.ones((365,2))
    theory_sun_daily = actual_sun_daily.copy()

    for i in range(1,366):
        if len( s_df.query('doy==@i'))==144:
            actual_sun_daily[i-1]  = (np.min(s_df.query('doy==@i').query('sun==1').timestamp),
                                    np.max(s_df.query('doy==@i').query('sun==1').timestamp))
            theory_sun_daily[i-1]  = (np.min(s_df.query('doy==@i').query('ze>0').timestamp),
                                   np.max(s_df.query('doy==@i').query('ze>0').timestamp))




    return actual_sun_daily, theory_sun_daily

def _prepareData_old_(sunny_df):
    s_df= sunny_df.assign(doy = lambda x : x.index.day_of_year)
    sun_daily = pd.DataFrame(index=np.arange(1,366),columns=np.unique(s_df.index.time))
    ze_daily  = sun_daily.copy()

    for i in range(1,366):
        if len( s_df.query('doy==@i'))==144:
            sun_daily.iloc[i-1] = s_df.query('doy==@i').sun
            ze_daily.iloc[i-1]  = s_df.query('doy==@i').ze

    sun_daily = sun_daily.astype(float).transpose()
    ze_daily  = ze_daily.astype(float).transpose()

    ze_bool = ze_daily>0

    return sun_daily, ze_bool




def _prepareTime_(sun_daily):
    import datetime

    my_day = datetime.date(2021, 1, 1)

    x_dt = [datetime.datetime.combine(my_day, t) for t in sun_daily.index]
    y_dt = [datetime.datetime(2021, 1, 1) + datetime.timedelta(day - 1) for day in sun_daily.columns]

    return x_dt, y_dt

def plotActualAgainstTheory(ax,sunny_df,color,zorder):
    actual_sun_daily, theory_sun_daily = _prepareData_(sunny_df)
    x_dt,y_dt = _prepareTime_(sunny_df)

    ax(y_dt,x_dt,ze_bool.astype(float),cmap='grey')
    fig,ax = plot.subplots()
    ax.fill_between(y_dt,actual_sun_daily[:,0],color=color,linewidth=0.5)
    ax.format(xrotation=0,ylocator=('hour', range(0, 24, 2)),
    #           yminorlocator=('minute', range(0, 60, 10)),
               yformatter='%H:%M',
               xformatter='concise',
               xlocator=('month',range(1,13,2))
    #           ylabel='UTC time'
              )


    return ax


def plotActualAgainstTheory_old(ax,sunny_df,color='mustard',alpha=1):

    sun_daily, ze_bool = _prepareData_old_(sunny_df)
    x_dt,y_dt = _prepareTime_(sun_daily)

    ax.contour(y_dt,x_dt,ze_bool.astype(float),cmap='grey')

    ax.pcolormesh(y_dt,x_dt,sun_daily.astype(float),levels=2,cmap=color,alpha=alpha)
    ax.format(xrotation=0,ylocator=('hour', range(0, 24, 2)),
              yminorlocator=('minute', range(0, 60, 10)),
              yformatter='%H:%M',
              xformatter='concise',
              ylabel='UTC time'
             )

    ax.plot(np.nan,np.nan,color='grey')
    ax.plot(np.nan,np.nan,color=color,alpha=alpha)
#    ax.legend()
    return ax


def plotFrac(ax,sunny_df,color):
    sun_daily, ze_bool = _prepareData_old_(sunny_df)
    sunFrac = sun_daily.sum()/ze_bool.sum()*100
    x_dt,y_dt = _prepareTime_(sun_daily)
    ax.plot(y_dt,sunFrac,color=color)
    ax.format(xrotation=0,
              xformatter='concise',
              )




def plotSolarElevation(ax,sunny_df,plotSolarLines=False,place='',color='C0'):
    summer_solstice = '2021-06-23'
    winter_solstice = '2021-12-22'


    ax.plot(sunny_df.loc[summer_solstice].az.values[0:141].astype(float),
                    sunny_df.loc[summer_solstice].angle.values[0:141].astype(float),
                    color=color,label=place)

    if plotSolarLines:
        ax.plot(sunny_df.loc[summer_solstice].az.values[0:141],
                sunny_df.loc[summer_solstice].ze.values[0:141],
                color='dark yellow',label='')
        ax.plot(sunny_df.loc[summer_solstice].az.values[0:141],
                sunny_df.loc[winter_solstice].ze.values[0:141],
                color='dark yellow',label='')

    ax.format(ylim=[0,60])
    return ax


#%%

def generateReport(sunny_df,name=''):
    default_places = _loadDefaults_()

    grid = [[1,2],
            [1,2],
            [1,3],
            [4,3],
            [4,5],
            [4,5]]

    fig,axs = plot.subplots(grid,sharey=False,sharex=False,figsize=(8,11))
    for c,place in zip(['light blue','light red'],default_places):
        ax = plotSolarElevation(axs[0],default_places[place],plotSolarLines=True,
                           color=c,place=place)
        #plotActualAgainstTheory(axs[1],default_places[place],color=c,zorder=
        #                        10)
        plotFrac(axs[3], default_places[place],color=c)


    __ = plotSolarElevation(axs[0],sunny_df,plotSolarLines=False,
                           color='green',place=name)
    axs[0].format(title='Topography',ylabel='angle')
    plotActualAgainstTheory_old(axs[1], default_places['Hjemme'],color='light blue',alpha=0.5)
    axs[1].set_title('Hjemme')

    plotActualAgainstTheory_old(axs[2], default_places['Lerkerinden'],color='light red',alpha=0.5)
    axs[2].set_title('Lerkerinden')
    plotActualAgainstTheory_old(axs[4],sunny_df,alpha=0.5,color='teal')
    axs[4].set_title(name)


    plotFrac(axs[3], sunny_df,color='teal')
    axs[3].format(ylabel='%',title='Effectiveness')


    axs[0].legend()

    axs.format(suptitle=name)
    return fig
