#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 14:19:02 2021

@author: kko033
"""

import numpy as np
import pandas as pd

import pysolar as ps
import earthpy.spatial as es
from shapely.geometry import Point
import rasterio as rio
import scipy.ndimage as nd
import geopandas as gpd
from datetime import datetime as dt
from pyproj import CRS



def _preparePoint_(lon,lat,buffer):
    geom=[Point(lon, lat)]
    # and put in a geoDataFrame
    gdf=gpd.GeoDataFrame(geometry=geom,crs='epsg:4326')

    circle = gdf.to_crs(epsg=3395).buffer(buffer*1e3)
    circle_gdf = gpd.GeoDataFrame({'buffer' : 'circle', 'geometry' : circle},crs = CRS('epsg:3395'))

    return circle_gdf    

def _prepareTime_(dates): 
    dates_UTC = dates.tz_convert('UTC')
    dateList = [pd.Timestamp(date) for date in dates_UTC]

    return dateList,dates_UTC 
    
def azimuth_topo(az,topo,topo_meta):
    """
    Returns the interpolated topography on a line from an angle (azimuth) from the north.


    Parameters
    ----------
    az : float
        azimuth angle
    topo : np ndarray
        topography
    topo_meta : dictionary
        metadata about the topography file

    Returns
    -------
    zi : np ndarray
        topography data along a straight line in an angle.

    """
    
    N = topo_meta['width']
    
    hos = np.cos(np.deg2rad(az-90))*N/2
    mot = np.sin(np.deg2rad(az-90))*N/2

    #-- Extract the line...
    # Make a line with "num" points...
    
    x0, y0 = N/2, N/2 
    x1, y1 = mot+N/2,hos+N/2
    x, y = np.linspace(x0, x1, np.int(N/2)), np.linspace(y0, y1, np.int(N/2))

    zi = nd.map_coordinates(topo, np.vstack((x,y)))    
    return zi

def calcSolar(lon,lat,dateList, dates_UTC,elev=0):
    """
    Calculate the solar angles on a lat lon location for given dates

    Parameters
    ----------
    lon : float
        longitude.
    lat : float
        latitude.
    dateList : list of datetime object
        dates for calculations
    dates_UTC : pandas daterange in UTC
        same as datelist.

    Returns
    -------
    sun_df : pd dataframe
        contains the azimuth and zenith angle of the sun for each timestep.

    """
    

    azimuth= np.array([ps.solar.get_azimuth(lat,lon,aa,elevation=elev) for aa in dateList])
    zenith = np.array([ps.solar.get_altitude(lat,lon,aa,elevation = elev) for aa in dateList])
    
    sun_df = pd.DataFrame(columns=dates_UTC,data=[azimuth,zenith],index=['azimuth','zenith']).transpose()
    
    return sun_df

    


def find_angle(Z,topo_meta,az):
    """
    find the steepest angle of the topography

    Parameters
    ----------
    Z : np array (x)
        2D topography - along a line.

    Returns
    -------
    angle : float
        steepest angle.

    """
    from scipy.signal import find_peaks
    
    N = topo_meta['width']
    
    x = np.arange(0,N)
    pks,_=find_peaks(Z,height=Z[0])
    angles = []
    for pk in pks:
        _angle = np.rad2deg(np.arctan((Z[pk]-Z[0])/(x[pk]*topo_meta['reso'])))
        angles.append(_angle)
    try: 
        angle = np.max(angles)
    except ValueError:
        angle = 0

    return angle


def pol2cart( phi):
    x = 1000 * np.cos(np.deg2rad(phi))
    y = 1000 * np.sin(np.deg2rad(phi))
    return(x, y)

    
def azimuth_matrix_factory(topo,topo_meta): 
    az_matrix = pd.DataFrame(index=np.arange(0,len(np.linspace(0,360,720))),columns=['azimuth','angle'])
    
    for ii,az in enumerate(np.linspace(0,360,720)): 
        _z = azimuth_topo(az,topo,topo_meta)
        angle = find_angle(_z,topo_meta,az)
        az_matrix.iloc[ii] = az,angle
    return az_matrix,_z[0]



def sunHours(lon,lat,dates,buffer=10,reso='low'):
    '''
    Calculutes and returns whether there is sun at a location taking 
    topography into account. 

    Parameters
    ----------
    lon : float
        longitude.
    lat : float
        latitude.
    dates : pd daterange
        daterange for which you want to calculate the sun.
    reso : string, optional
        resolution of topography. The default is 'low'.

    Returns
    -------
    topo : np ndarray
        topography.
    sunDf : pd dataframe
        dataframe with sun parameters.
    sunny_df : pd dataframe
        dataframe with parameters important for sun in a location.

    '''
    circle = _preparePoint_(lon,lat,buffer)
    dateList,datesUTC =  _prepareTime_(dates)
    
    if 'low' in reso:
        with rio.open('dtm50/data/dtm50_3395.tif') as dtm50_ll: 
            cropped,cropped_meta = es.crop_image(dtm50_ll,circle.geometry)
            cropped_meta['reso'] = 50
    elif 'hi' in reso: 
        with rio.open('dtm10/data/dtm10_67m1_2_10m_3395_3.tif') as dtm10_ll: 
            cropped, cropped_meta = es.crop_image(dtm10_ll,circle.geometry)
            cropped_meta['reso'] = 10

    topo = cropped.squeeze()        
    # The bottom of the ocean is not important
    topo[topo<0] = 0
   
    print('preparing azimuth matrix ...')
    az_mtrx,elev = azimuth_matrix_factory(topo,cropped_meta)
    print('finding sun hours ...')
    # put in a dataframe for easier indexing
    Bergen_sun=pd.read_csv('Bergen_sun.csv')
    sunny_df = pd.DataFrame(index=datesUTC,columns=['sun','angle','ze','az'])
    for ii,time in enumerate(datesUTC):
        tix=np.argmin(abs(Bergen_sun.Time_number-dt.timestamp(time)))
        az = Bergen_sun['Azimuth'].iloc[tix]
        ze = Bergen_sun['Height_angle'].iloc[tix]
       
        tix=np.argmin(abs(az_mtrx.azimuth-az))
        if ze>=az_mtrx.angle[tix]:
            sunny_df.iloc[ii]['sun'] = True
        else: 
            sunny_df.iloc[ii]['sun'] = False
            
        sunny_df.iloc[ii]['angle'] = az_mtrx.angle[tix]
        sunny_df.iloc[ii]['ze']    = ze
        sunny_df.iloc[ii]['az']    = az
    
    # for ii,time in enumerate(datesUTC):
    #     t = dateList[ii]
    #     tix=np.argmin(abs(Bergen_sun.Time_number-dt.timestamp(time)))
    #     az = Bergen_sun['Azimuth'].iloc[tix]
    #     ze = Bergen_sun['Height_angle'].iloc[tix]

    #     _ang_ix = az_mtrx.azimuth.sub(az).astype(float).abs().idxmin()
    #     angle = az_mtrx.loc[_ang_ix].angle
        
    #     if angle<=ze:
    #         sunny_df.loc[time]['sun'] = True
    #     else: 
    #         sunny_df.loc[time]['sun'] = False
    #     sunny_df.loc[time]['angle'] = angle
    #     sunny_df.loc[time]['ze']    = ze
    #     sunny_df.loc[time]['az']    = az
    print('finsihed')
    return topo, az_mtrx,sunny_df




