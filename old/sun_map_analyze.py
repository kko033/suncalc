#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  5 08:53:32 2021

@author: kko033
"""

import xarray as xr
import proplot as plot
import rioxarray as rio

sunMap = xr.open_dataset('sun_map')
sunMap = sunMap.rename_vars({'__xarray_dataarray_variable__':'sun'})

# need to reproject  

sunMap_latlon = sunMap.rio.write_crs('EPSG:3395').transpose('y','x').rio.reproject('EPSG:4326')


#%% COASTLINE
coastline_ = rio.open_rasterio('dtm50/data/dtm50_3395.tif')
coastline = coastline_.rio.reproject('EPSG:4326').sel(x=slice(sunMap_latlon.x.min(),sunMap_latlon.x.max()),
                                                      y=slice(sunMap_latlon.y.max(),sunMap_latlon.y.min()))
coastline.name='topo'
#%% combine the two datasets

da_coast = coastline.interp_like(sunMap_latlon)
da = xr.merge([da_coast,sunMap_latlon.sun])

da = da.where(da.topo>0)


#%% 
import matplotlib
current_cmap = matplotlib.cm.get_cmap('YlGnBu_r')
current_cmap.set_bad(color='gray')


fig,ax = plot.subplots(width=10)
ax.pcolormesh(da.sun.squeeze(),colorbar='r',cmap=current_cmap,levels=20,
              colorbar_kw = {'label':'fraction of incoming theoretical solar radiation between JAN-MAY 14:00 to 23:00'})
ax.contour(da.sun.squeeze(),levels=[0.85],colors='k',linewidth=0.3)
ax.format(ylabel='',xlabel='')

