#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 21:18:46 2021

@author: tmbaumann
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pickle as p
from tictoc import tic,toc
from sunhours_calc import sunHours
#Bergen_sun=pd.read_csv('Bergen_sun.csv')
#%%
#a=pd.read_csv('Bergen_sun.csv')
test_places = {'Lerkerinden' : (60.34208077176912, 5.362093098116615),
               'Hjemme'      : (60.378224338465834, 5.327000241043094),
              }
dates=pd.date_range('01-01-2021','31-12-2021',freq='10T',tz='Europe/Oslo')

default_places = {}


for place in test_places: 
    lat,lon = test_places[place]
    
    tic()
    topo, topo_angle,sunny_df=sunHours(lon,lat,dates,buffer=10,reso='high')
    toc()

    sunny_df.to_pickle(place)
#%%    save the variables in a okay way 

