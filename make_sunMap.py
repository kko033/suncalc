#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 11 20:34:25 2021

@author: tmbaumann
"""
import pandas as pd
from sun_hours_calc_for_map import sunHours_map

from shapely.geometry import Point
import geopandas as gpd
from pyproj import CRS
import xarray as xr
import numpy as np 

import sys
from tictoc import tic,toc,progressBar
### Create map

t = xr.open_rasterio('dtm50/data/dtm50_3395.tif')

#%% First, prepare bounding latitudes: condvert from latlon to 3395

lomax = 5.487
lomin = 5.172
lamax = 60.464
lamin = 60.255

coords = []


geom=[Point(lomin, lamin),
      Point(lomax,lamin),
      Point(lomin,lamax),
      Point(lomax,lamax)]
# and put in a geoDataFrame
gdf=gpd.GeoDataFrame(geometry=geom,crs='epsg:4326')
circle = gdf.to_crs(epsg=3395)

def getXY(c):
    return c.geometry.x,c.geometry.y

x,y  = getXY(circle)

t_b = t.sel(y=slice(y.max(),y.min()),x=slice(x.min(),x.max()))




#%%


dates=pd.date_range('01-01-2021','31-05-2021',freq='10T',tz='Europe/Oslo')

#this finds the max sun hours regardless of location
Bergen_sun=pd.read_csv('Bergen_sun.csv').set_index('Unnamed: 0')
Bergen_sun.index = pd.to_datetime(Bergen_sun.index)
index1=((Bergen_sun.index.month>=1) & (Bergen_sun.index.month<=5)
        & (Bergen_sun.index.hour>=14) & (Bergen_sun.index.hour<=23)
        & (Bergen_sun.Height_angle>0))
full_sun=index1.sum()

#%%
i = 0

sun = np.ones((len(t_b.x),len(t_b.y)))*np.nan

s_len = len(t_b.x)*len(t_b.y)

k = 0
#t_c = t_b.where((t_b>1) & (t_b<250))
for ix,x in enumerate(t_b.x.values):
    i = 0
    print('{}/{}'.format(k,len(t_b.x.values)))
    for iy,y in enumerate(t_b.y):
        progressBar(i/len(t_b.y))
        topo, topo_angle,sunny_df=sunHours_map(x,y,dates,buffer=10,reso='low')
               
        sun[ix,iy] = sunny_df.query('index.dt.hour>=14 and index.dt.hour<=23').sun.sum()/full_sun
        i+=1
    k+=1
    
    
#%%
sun_da = xr.DataArray(
             sun,
             coords = {
                 'x':t_b.x,
                 'y':t_b.y},
             dims = ['x','y'])

sun_da.to_netcdf('sun_map.nc')