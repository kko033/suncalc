#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 21:58:41 2021

@author: kko033
"""

import proplot as plot
import pandas as pd
import numpy as np

import rioxarray as rio
import xarray as xr

def _loadDefaults_():
    import glob
    _places = glob.glob('./default_data/*.p')
    default_places = {}
    for place in _places:
        namePlace = place.split('/')[-1][0:-2]
        default_places[namePlace] = pd.read_pickle(place)
    return default_places


def _prepareData_(sunny_df):
    s_df= sunny_df.assign(doy = lambda x : x.index.day_of_year)

    sun_daily = pd.DataFrame(columns=np.unique(s_df.doy),index=np.unique(s_df.index.time))
    ze_daily  = sun_daily.copy()

    for i in np.unique(s_df.doy):
        _stuff = s_df.query('doy==@i').assign(time=lambda x:x.index.time).set_index('time')
        _stuff = _stuff[~_stuff.index.duplicated(keep='first')]

        sun_daily[i] = _stuff.sun
        ze_daily[i]= _stuff.ze



    ze_bool = ze_daily>0
    return sun_daily.astype(bool), ze_bool.astype(bool)




def _prepareTime_(sun_daily):
    import datetime

    my_day = datetime.date(2021, 1, 1)

    x_dt = [datetime.datetime.combine(my_day, t) for t in sun_daily.index]
    y_dt = [datetime.datetime(2021, 1, 1) + datetime.timedelta(day - 1) for day in sun_daily.columns]

    y_dt = pd.to_datetime(y_dt)

    return x_dt, y_dt


def plotActualAgainstTheory(ax,sunny_df,color='mustard',alpha=1):
    time=sunny_df.index.values
    time=pd.to_datetime(time)
    time=time.tz_localize(tz='UTC')
    tt=time.tz_convert('Europe/Oslo')
    sunny_df.index=tt

    sun_daily, ze_bool = _prepareData_(sunny_df)

    x_dt,y_dt = _prepareTime_(sun_daily)

    ax.contour(y_dt,x_dt,ze_bool.astype(float),cmap='grey')

    ax.pcolormesh(y_dt,x_dt,sun_daily.astype(float),levels=2,cmap=color,shading='auto')
    ax.format(xrotation=0,ylocator=('hour', range(0, 24, 2)),
              yminorlocator=('minute', range(0, 60, 10)),
              yformatter='%H:%M',
              xformatter='concise',
              ylabel='Local time'
             )

    ax.plot(np.nan,np.nan,color='grey')
    ax.plot(np.nan,np.nan,color=color)
#    ax.legend()
    return ax


def plotFrac(ax,sunny_df,color):
    sun_daily, ze_bool = _prepareData_(sunny_df)
    zebsum=ze_bool.sum()
    zebsum[zebsum==0]=np.nan
    sunFrac = sun_daily.sum()/zebsum*100
    x_dt,y_dt = _prepareTime_(sun_daily)
    ax.plot(pd.to_datetime(y_dt),sunFrac,color=color)
    ax.format(xrotation=0,
              xformatter='concise',
              )




def plotSolarElevation(ax,sunny_df,plotSolarLines=False,place='',color='C0'):
    summer_solstice = '2021-06-23'
    winter_solstice = '2021-12-22'
    time=sunny_df.index.values
    time=pd.to_datetime(time)
    time=time.tz_localize(tz='UTC')
    sunny_df.index=time

    ax.plot(sunny_df.loc[summer_solstice].az.values[0:141].astype(float),
                    sunny_df.loc[summer_solstice].angle.values[0:141].astype(float),
                    color=color,label=place)

    if plotSolarLines:
        ax.plot(sunny_df.loc[summer_solstice].az.values[0:141].astype(float),
                sunny_df.loc[summer_solstice].ze.values[0:141].astype(float),
                color='dark yellow',label='')
        ax.plot(sunny_df.loc[summer_solstice].az.values[0:141].astype(float),
                sunny_df.loc[winter_solstice].ze.values[0:141].astype(float),
                color='dark yellow',label='')

    ax.format(ylim=[0,60])
    return ax


#%% Functions :) For map-plotting
def readSunData(): 
    """
    Read and prepare (reproject and rename variables) the sun data (the map)

    Returns
    -------
    sunMap_latlon : xarray dataset
        sunMap in latlon

    """
    sunMap = xr.open_dataset('sun_map.nc')
    sunMap = sunMap.rename_vars({'__xarray_dataarray_variable__':'sun'})
    
    # need to reproject  
    
    sunMap_latlon = sunMap.rio.write_crs('EPSG:3395').transpose('y','x').rio.reproject('EPSG:4326')

    return sunMap_latlon

def readAndPrepareSunandCoast(): 
    """
    Load the coastline, the sunData (from the making of the map) and 
    mask the data for the ocean

    Returns
    -------
    da : xarray dataset
        with topography and the masked sun values.

    """
    sunMap_latlon = readSunData()
    coastline_ = rio.open_rasterio('dtm50/data/dtm50_3395.tif')
    coastline = coastline_.rio.reproject('EPSG:4326').sel(x=slice(sunMap_latlon.x.min(),sunMap_latlon.x.max()),
                                                      y=slice(sunMap_latlon.y.max(),sunMap_latlon.y.min()))
    coastline.name='topo'
    
    da_coast = coastline.interp_like(sunMap_latlon)
    da = xr.merge([da_coast,sunMap_latlon.sun])    
    da = da.where(da.topo>0)

    return da.sortby('y')

def plotMap(ax,lat=60.327, lon = 5.346,withPoints = True):
    """
    Plot the sun map

    Parameters
    ----------
    ax : axis subcontainer
        axis to plot the plot.
    lat : float, optional
        Latitude of POI. The default is 60.327.
    lon : float, optional
        Longitude of POI. The default is 5.346.
    withPoints : Bool, optional
        If you want to add arrows with values for the POI and my and Till's place. The default is True.

    Returns
    -------
    None.

    """
    Klat,Klon = 60.378150355222516, 5.327024546065821
    Tlat,Tlon = 60.34214215250333, 5.361950387608049
    
    da = readAndPrepareSunandCoast()
    
    
    import matplotlib
    current_cmap = matplotlib.cm.get_cmap('YlGnBu_r')
    current_cmap.set_bad(color='gray')
    
  
    ax.contourf(da.sun.squeeze(),colorbar='r',cmap=current_cmap,levels=20,
                  colorbar_kw = {'label':'fraction of incoming radiation'})
    ax.contour(da.sun.squeeze(),levels=[0.85],colors='k',linewidth=0.3)
    if withPoints: 
        ax.scatter(Klon,Klat,c='None',edgecolor='k',lw = 2,s=50)
        ax.scatter(Tlon,Tlat,c='None',edgecolor='k',lw=2,s=50,zorder=5)
        ax.scatter(lon,lat,c='None',edgecolor='cerise',lw=2,s=50,zorder=5)
        
        i = 1
        
        for x,y in zip([Klon,Tlon,lon],[Klat,Tlat,lat]): 
            v = np.round(da.sun.sel(x=x,y=y,method='nearest').values,2).squeeze()
            ax.annotate(v,
                xy=(x,y), xycoords='data',
                xytext=(i*0.15,0.05), textcoords='axes fraction',
                arrowprops=dict(arrowstyle="-|>",
                                connectionstyle="angle3"),
                bbox=dict(boxstyle="round", fc="w"),zorder=6
                )
            i=i+1
        
    ax.format(ylabel='',xlabel='')
    ax.set_title('JAN-MAY 14:00-23:00')

    return 


###%%



def generateReport(sunny_df,name=''):
    """
    Generate standard report for the sunhours calc

    Parameters
    ----------
    sunny_df : dataframe
        output from the sunhours_calc.
    name : string, optional
        Name of the place; for better saving of reports and title. The default is ''.

    Returns
    -------
    fig : figure
        Convenient if you want to save it.


    """
    default_places = _loadDefaults_()

    grid = [[1,2],
            [1,2],
            [1,3],
            [4,3],
            [4,5],
            [4,5]]

    fig,axs = plot.subplots(grid,sharey=False,sharex=False,figsize=(8,11))
    for c,place in zip(['dark sky blue','light red'],default_places):
        ax = plotSolarElevation(axs[0],default_places[place],plotSolarLines=True,
                           color=c,place=place)
        #plotActualAgainstTheory(axs[1],default_places[place],color=c,zorder=
        #                        10)
        plotFrac(axs[3], default_places[place],color=c)


    __ = plotSolarElevation(axs[0],sunny_df,plotSolarLines=False,
                           color='teal',place=name)
    axs[0].format(title='Topography',ylabel='angle')
    plotActualAgainstTheory(axs[1], default_places['Hjemme'],color='dark sky blue',alpha=0.5)
    axs[1].set_title('Hjemme')

    plotActualAgainstTheory(axs[2], default_places['Lerkerinden'],color='light red',alpha=0.5)
    axs[2].set_title('Lerkerinden')
    plotActualAgainstTheory(axs[4],sunny_df,alpha=0.5,color='teal')
    axs[4].set_title(name)


    plotFrac(axs[3], sunny_df,color='teal')
    axs[3].format(ylabel='%',title='Effectiveness')


    axs[0].legend(ncols=2)

    axs.format(suptitle=name)
    return fig

def generateReport_w_map(sunny_df,lat,lon,name=''): 
    """
    Generates a more fancy report than the original (with map!) 

    Parameters
    ----------
    sunny_df : dataframe
        output from the sunhours_calc.
    lat : float
        latitude of POI.
    lon : float
        longitude of POI.
    name : string, optional
        Name of the place; for better saving of reports and title. The default is ''.

    Returns
    -------
    fig : figure
        Convenient if you want to save it.

    """
    
    default_places = _loadDefaults_()

    grid = [[1,2],
            [1,2],
            [3,4],
            [3,4],
            [5,6],
            [5,6]]
    
    fig,axs = plot.subplots(grid,sharey=False,sharex=False,figsize=(8,11))
    __ =plotMap(axs[0],lat,lon)
    
    plotActualAgainstTheory(axs[1], default_places['Hjemme'],color='dark sky blue',alpha=0.5)
    axs[1].set_title('Hjemme')
    
    __ = plotSolarElevation(axs[2],sunny_df,plotSolarLines=False,
                           color='teal',place=name)
    axs[2].format(title='Topography',ylabel='angle')


    plotActualAgainstTheory(axs[3], default_places['Lerkerinden'],color='light red',alpha=0.5)
    axs[3].set_title('Lerkerinden')
    plotActualAgainstTheory(axs[5],sunny_df,alpha=0.5,color='teal')
    axs[5].set_title(name)


    plotFrac(axs[4], sunny_df,color='teal')
    axs[4].format(ylabel='%',title='Effectiveness')



    for c,place in zip(['dark sky blue','light red'],default_places):
        __ = plotSolarElevation(axs[2],default_places[place],plotSolarLines=True,
                           color=c,place=place)
        
        plotFrac(axs[4], default_places[place],color=c)
    

    axs[2].legend(ncols=2)

    axs.format(suptitle=name)
    return fig


    
    