Calculate the sun at a location in Bergen wrt topography. Topography files are downloaded from https://hoydedata.no/LaserInnsyn/, at both 50 and 10 m resolution. 

Major calculations are done in `sunHours_calc.py` while there are some plotting functions in `sunHours_plot.py`. `generate_reports.py` generates standard reports. 

--- 
SunCalc depends on a lot of other packages:
- scipy
- pysolar
- earthpy
- rasterio
- geopandas
- pyproj
- shapely
- numpy 
- pandas
- proplot
- matplotlib
