#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 15:21:42 2021

@author: kko033
,"""
import pandas as pd
from sunhours_calc import sunHours


name = '8_Renen'

lat,lon =60.3424910962636, 5.377067027792322

dates=pd.date_range('01-01-2021','31-12-2021',freq='10T',tz='Europe/Oslo')

report_type='fancy' # normal or fancy or map_only


#%% 
topo, topo_angle,sunny_df=sunHours(lon,lat,dates,buffer=10,reso='hi')


#%%
from sunhours_plot import generateReport,generateReport_w_map, plotMap


def reports(report_type='normal'):
    if 'normal' in report_type:
        fig=generateReport(sunny_df.astype(float),name)
        fig.save('reports/'+name)

    elif 'fancy' in report_type:
        fig = generateReport_w_map(sunny_df,lat,lon,name)
        fig.save('reports/{}_w_map'.format(name),dpi=1500)

    elif 'map_only' in report_type:
        import proplot as plot
        fig,ax = plot.subplots(width=5)
        plotMap(ax,lat,lon,withPoints=False)

    return fig

reports(report_type)
