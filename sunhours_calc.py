#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 14:19:02 2021

@author: kko033
"""

import numpy as np
import pandas as pd

import pysolar as ps
import earthpy.spatial as es
from shapely.geometry import Point
import rasterio as rio
import scipy.ndimage as nd
import geopandas as gpd
from pyproj import CRS

#--------------- Data preparation ---------------

def _preparePoint_(lon,lat,buffer):
    geom=[Point(lon, lat)]
    # and put in a geoDataFrame
    gdf=gpd.GeoDataFrame(geometry=geom,crs='epsg:4326')

    circle = gdf.to_crs(epsg=3395).buffer(buffer*1e3)
    circle_gdf = gpd.GeoDataFrame({'buffer' : 'circle', 'geometry' : circle},crs = CRS('epsg:3395'))

    return circle_gdf    

def _prepareTime_(dates): 
    dates_UTC = dates.tz_convert('UTC')
    dateList = [pd.Timestamp(date) for date in dates_UTC]

    return dateList,dates_UTC 

# ----- Debugplot

def debugPlot(topo,x,y,N,zi,az,topo_meta): 
    import proplot as plot
    a,x_pk = find_angle(zi,topo_meta,az)

    fig,ax = plot.subplots(includepanels=True)
    ax[0].imshow(topo,levels=30,colorbar='b')
    ax[0].scatter(N/2,N/2)
    ax.plot(x,y)
    paxs = ax.panel('b')
    paxs.plot(zi)
    ax.format(title=az)
#---------------Topography and angole calculatiosn ---------------


def azimuth_topo(az,topo,topo_meta):
    """
    Returns the interpolated topography on a line from an angle (azimuth) from the north.


    Parameters
    ----------
    az : float
        azimuth angle
    topo : np ndarray
        topography
    topo_meta : dictionary
        metadata about the topography file

    Returns
    -------
    zi : np ndarray
        topography data along a straight line in an angle.

    """
    
    N = topo_meta['width']
    
    hos = np.cos(np.deg2rad(az-90))*N/2
    mot = np.sin(np.deg2rad(az-90))*N/2

    #-- Extract the line...
    # Make a line with "num" points...
    
    x0, y0 = N/2, N/2 
    x1, y1 = mot+N/2,hos+N/2
    x, y = np.linspace(x0, x1, np.int(N/2)), np.linspace(y0, y1, np.int(N/2))

    zi = nd.map_coordinates(topo, np.vstack((x,y)))   
    #debugPlot(topo,x,y,N,zi,az-0,topo_meta)
    
   
    
    return zi

def find_angle(Z,topo_meta):
    """
    find the steepest angle of the topography

    Parameters
    ----------
    Z : np array (x)
        2D topography - along a line.
    topo_meta : dictionary
        meta informatio about topography - resolution  

    Returns
    -------
    angle : float
        steepest angle.
    xs : 
        
    """
    from scipy.signal import find_peaks
    
    N = topo_meta['width']
    x = np.arange(0,N)
    pks,_=find_peaks(Z,height=Z[0])
    angles = []
    for pk in pks:
        _angle = np.rad2deg(np.arctan((Z[pk]-Z[0])/(x[pk]*topo_meta['reso'])))
        angles.append(_angle)
    try:
        angle = np.max(angles)
        xs = pks[np.argmax(angles)]    
        if xs < 2: 
            angle = np.max(angles[2:])
            xs = pks[2:][np.argmax(angles[2:])]
    except ValueError:
        angle = 0
        xs = np.nan
    return angle,xs



def azimuth_matrix_factory(topo,topo_meta): 
    '''
    Create a matrix with the steepest angle of topgraphy in a direction
    

    Parameters
    ----------
    topo : nd array
        topogrpahy.
    topo_meta : dict
        meta data about the topography.

    Returns
    -------
    az_matrix : np.ndarray
        topography along a straight line in all azimuth angles.
    z : float
        height of your location.

    '''
    ar = np.arange(34,326,0.5)
    
    az_matrix = pd.DataFrame(index=np.arange(0,len(ar)),columns=['azimuth','angle','x_dist'],
                             dtype=float)
    for ii,az in enumerate(ar): 
        z = azimuth_topo(az,topo,topo_meta)
        angle,x = find_angle(z,topo_meta)
        az_matrix.iloc[ii] = az,angle,x
    return az_matrix,z[0]


#--------------- Solar calculation ---------------

def calcSolar(lon,lat,dateList, dates_UTC,elev=0):
    """
    Calculate the solar angles on a lat lon location for given dates

    Parameters
    ----------
    lon : float
        longitude.
    lat : float
        latitude.
    dateList : list of datetime object
        dates for calculations
    dates_UTC : pandas daterange in UTC
        same as datelist.

    Returns
    -------
    sun_df : pd dataframe
        contains the azimuth and zenith angle of the sun for each timestep.

    """
    azimuth= np.array([ps.solar.get_azimuth(lat,lon,aa,elevation=elev) for aa in dateList])
    zenith = np.array([ps.solar.get_altitude(lat,lon,aa,elevation = elev) for aa in dateList])
    
    sun_df = pd.DataFrame(columns=dates_UTC,data=[azimuth,zenith],index=['azimuth','zenith']).transpose()
    
    return sun_df

#---------------Main function ---------------


def sunHours(lon,lat,dates,buffer=10,reso='hi'):
    '''
    Calculates and returns whether there is sun at a location taking 
    topography into account. 

    Parameters
    ----------
    lon : float
        longitude.
    lat : float
        latitude.
    dates : pd daterange
        daterange for which you want to calculate the sun.
    buffer : float
        how large the radius surrounding the point (given in km) 
        you want to consider the topography. The default is 10. 
    reso : string, optional
        resolution of topography. The default is 'hi'.

    Returns
    -------
    topo : np ndarray
        topography.
    sunDf : pd dataframe
        dataframe with sun parameters.
    sunny_df : pd dataframe
        dataframe with parameters important for sun in a location.

    '''
    circle = _preparePoint_(lon,lat,buffer)
    dateList,datesUTC =  _prepareTime_(dates)
    
    if 'low' in reso:
        with rio.open('dtm50/data/dtm50_3395.tif') as dtm50_ll: 
            cropped,cropped_meta = es.crop_image(dtm50_ll,circle.geometry)
            cropped_meta['reso'] = 50
    elif 'hi' in reso: 
        with rio.open('dtm10/data/dtm10_67m1_2_10m_3395_3.tif') as dtm10_ll: 
            cropped, cropped_meta = es.crop_image(dtm10_ll,circle.geometry)
            cropped_meta['reso'] = 10

    topo = cropped.squeeze()        
    # The bottom of the ocean is not important
    topo[topo<0] = 0
   
    # read in the sun curves
    Bergen_sun=pd.read_pickle('Bergen_sun.p')

    print('preparing azimuth matrix ...')
    az_mtrx,elev = azimuth_matrix_factory(topo,cropped_meta)
    print('finding sun hours ...')
    # put in a dataframe for easier indexing
    
    sunny_df = pd.DataFrame(index=datesUTC,columns=['sun','angle','ze','az'],
                            dtype=float)
    sunny_df['az'] = Bergen_sun['Azimuth']
    sunny_df['ze'] = Bergen_sun['Height_angle']
    
    # find the closest azimuth topography to real topography
    min_za = find_smallest_diff(az_mtrx.azimuth.values.astype(float),
                                sunny_df.az.values.astype(float))
    
    # how often it is sun 
    s = sunny_df.ze.values > az_mtrx.loc[min_za].angle.values
    sunny_df['sun'] = s
    sunny_df['angle']  = az_mtrx.loc[min_za].angle.values
    print('finished')
    return topo, az_mtrx,sunny_df


def find_smallest_diff(A,B):
    '''
    Find the smallest difference between A and B and return the indices. 

    Parameters
    ----------
    A : np.array
        array of floats.
    B : np.array
        array of other floats.

    Returns
    -------
    diff: np.array
        array of indices of the nearest A for each B.

    '''
    [X,Y] = np.meshgrid(A,B);
    diff = abs(X-Y)
    return np.argmin(diff,axis=1)
