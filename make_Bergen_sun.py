#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 20:59:31 2021

@author: tmbaumann
"""
import numpy as np
import pandas as pd
import pysolar as ps
from datetime import datetime as dt

lat=60.378224338465834
lon=5.327000241043094
dates=pd.date_range('01-01-2021','12-31-2021',freq='10T',tz='Europe/Oslo')
dates_UTC = dates.tz_convert('UTC')
dateList = [pd.Timestamp(date) for date in dates_UTC]

az=np.ones(len(dateList))
ze=az.copy()
tnum=az.copy()
elev=0
for ii,t in enumerate(dateList):
    tnum[ii]=dt.timestamp(dates_UTC[ii])
    az[ii] = ps.solar.get_azimuth(lat,lon,t,elev)
    ze[ii] = ps.solar.get_altitude(lat,lon,t,elev)
    
#%%
Bergen_sun=pd.DataFrame(data=[tnum,az,ze],index=['Time_number','Azimuth','Height_angle'],columns=dates_UTC).transpose()
Bergen_sun.to_pickle('Bergen_sun.p')